<?php get_header(); ?>

<div id="content">
	
	<div class="row">
		<div class="column column-10">
            
            <?php $data = apply_filters('bluenoc_initiateQuerySetupFilter'); ?>
			
			<div class="title"> 
				<h4><?= get_cat_name($data['currentCategoryID']) ?></h4> 
                <form role="search" method="get" id="searchform" action="<?php bloginfo('url'); ?>">
                    <?= $data['hiddenInputStr'] ?>
                    <input class="item_searchbox" type="text" placeholder="Search..." value="<?= $data['searchStr'] ?>" name="s" id="s" />
                    <input class="item_searchbox_submit" type="submit" id="searchsubmit" value="" />
                </form>
			</div>
			
			<div class="body">
                <?php while ($data['theSearch']->have_posts()) : $data['theSearch']->the_post() ?>
                    <div class="item">
                        <div class="itemTitle">
                            <div class="collapseButton">+</div>
                            <?php the_title(); ?>
                        </div>
                        
                        <div class="itemBody">
                            <?php 
							
								$itemContent = get_extended($post->post_content);
								$itemContentMain = apply_filters('the_content', $itemContent['main']);
								$itemContentExtended = apply_filters('the_content', $itemContent['extended']);

								echo $itemContentMain;

								//Cases for each category
								switch ( get_category( $data['currentCategoryID'] )->slug ) 
								{
									case "manuals":
										echo '<div class="guide">';
											echo '<a class="button" href="#' . get_the_id() . '">Οδηγός</a>';
											
											echo '<di$v class="body" id="' . get_the_id() . '">';
												echo $itemContentExtended;
											echo '</div>';
										echo '</div>';
										break;
									case "services":
										$options = get_post_custom_values("option");
										$link = get_post_custom_values("access")[0];
										
										foreach($options as $option)
										{
											if ($option == "guide")
											{
												echo '<div class="guide">';
													echo '<a class="button" href="#' . get_the_id() . '">Οδηγός</a>';
													
													echo '<di$v class="body" id="' . get_the_id() . '">';
														echo $itemContentExtended;
													echo '</div>';
												echo '</div>';
											}
										}
										break;
									
									default:
										break;
								}
        
							?>
                        </div>
                    </div>
                <?php endwhile;?>
                <?php wp_reset_postdata(); ?>
                
                <?= $data['errorMsg'] ?>
                
                <?php if ($data['theSearch_page_num_max'] > 1): ?>
                    <div class="cardButton link">
                        <a href="<?= $data['next_page_URL'] ?>">next</a>	
                    </div>	
                    <div class="cardButton link right">
                        <a href="<?= $data['prev_page_URL'] ?>">previous</a>	
                    </div>	
                <?php endif; ?>
                
			</div> <!-- end of body -->
			
		</div> <!-- end of column column 10 -->
        
	</div> <!-- end of row -->
	
</div> <!-- end of content -->

<?php get_sidebar(); ?>

<?php get_footer(); ?>
