# Versions #

### 1.0.x ###

* Misc: Occasional maintence and updates beyond this point.

### 1.0.0 ###

* Misc: Website is now at its final version.

### 0.9.x ###

* Misc: SEO Optimizations and Google analytics.
* Misc: Minification of front-end scripts and media content.
* Misc: Enable caching and cloudflare goodies.

### 0.9.0 ###

* Misc: Website is now entering beta stage and it is public to everyone. 

### 0.8.x ###

* Misc: More bug fixing and polish.
* Misc: Security improvements.

### 0.8.0 ###

* Misc: Website is now entering alpha stage. Most of the functinoality is there.

### 0.7.x ###

* Misc: Final content (text, images etc) is added.

### 0.7.0 ###

* Misc: Code cleanup and bug fixing.

### 0.6.x ###

* Misc: Any remaining approved suggestions are implemented here.

### 0.6.3 ###

* Feature: Additional responsive rules for portrait orientation.

### 0.6.2 ###

* Feature: Users can post comments in announcements via Disqus platform.

### 0.6.1 ###

* Feature: Sitemap has it's own dedicated page template.

### 0.6.0 ###

* Fix: Footer adjusts to minimum window height if there is not enough content to push it down at the bottom of the screen.
* Feature: Cards get a new small design pass. Information like author and date posted are now implemented.

### 0.5.9 ###

* Feature: New network map is replacing the old one. The new network map is implementing the following features:
            -Wifi ranges are represented as spheres and user can toggle them.
            -Lines represent a connection between nodes. 
            -Each node represents a computer in a network. 
            -Nodes are grouped in subnetworks and user is able to toggle them.
            -Each subnetwork has its own unique color that is used in lines (connections) and nodes (computers).
            -The map features both buildings and the user is able to transition between them.
* Fix: Network map renderer factors the responsive rule of 1440px.

### 0.5.8 ###

* Feature: Card statistics on the homepage (server statuses, load etc).

### 0.5.7 ###

* Feature: Activity hub - Social feed intergration

### 0.5.6 ###

* Feature: Contact form is now functional and gets its first design pass

### 0.5.5 ###

* Feature: Helpdesk is now intergraded to the site and gets its first design pass

### 0.5.4 ###

* Feature: Sidebar fetches the announcements dynamically through RESTful

### 0.5.3a - current ###

* Fix: Styles responsible for categories are now loaded only on pages with categories.

### 0.5.3 ###

* Feature: Guides' pages are now implemented.
* Feature: All categories are unified!
* Feature: New design pass on the category search bar.
* Feature: Unified frame in-page popup (Guides and Services will take full advantage)
* Misc: Major code cleanup

### 0.5.2 ###

* Feature: Services' pages are now implemented.

### 0.5.1c ###

* Fix: Homepage info cards are now fetched dynamically through category.

### 0.5.1b ###

* Fix: Pagination is now taking into account all posts of the searched string.

### 0.5.1 ###

* Fix: Homepage slider at sub 600px responsive rule.
* Feature: FAQs' pages are now implemented.
* Misc: Removed border from card submit button class.

### 0.5.0 ###

* Feature: Added submit button class to cards
* Feature: Homepage articles now make proper use of <!--more--> tag

### 0.4.9b ###

* Fix: 404 title being too high

### 0.4.9 ###

* Feature: Added Footer menu which is in footer (duh)!
* Feature: Added more ripple animations
* Feature: Network map viewport is updated and camera can be controlled!
* Fix: Sidebar button will no longer hide if sidebar is shown
* Misc: 404 page redesign

### 0.4.8b ###

* Fix: Sad face width on 404 page adjusted

### 0.4.8a ###

* Fix: Width scroll on 404 page removed

### 0.4.8 ###

* Feature: Added the 404 page.
* Feature: Added background texture
* Feature: Added first implementation of slider
* Misc: Code cleanup

### 0.4.7a ###

* Fix: Activity hub toggle button wouldn't show up after scroll on sizes between 928 - 1440px

### 0.4.7 ###

* Tabs without submenus are now clickable.
* Menu logo has a refresh animation on click and hold.
* Menu logo resources use sprites, to avoid blinking on hover while the image is being loaded up.
* Back to top button added and will show up below 10px scrolled.
* Activity hub toggle button will now fade out if no mouse hover or scroll is detected within 5 seconds.
* Navbar got a small new design pass.
* Navbar dropdown will wait for a couple of ms in order to avoid incorrect hide() calls (mainly ie11 problem).
