<?php get_header(); ?>

<?php

	$slides_q = new WP_Query( array("tag" => "homepage") );
	$slides;

	$i = 0;
	if ( $slides_q->have_posts() ) :
		while ($slides_q->have_posts()) : $slides_q->the_post();
		$slides[$i]['title'] = strip_tags( get_the_title() );
		$slides[$i]['content'] = strip_tags( get_the_content() );
		
		$slides[$i]['mcontainer_left'] = strip_tags( get_post_custom_values("mcontainer_left")[0] );
		$slides[$i]['mcontainer_center'] = strip_tags( get_post_custom_values("mcontainer_center")[0] );
		$slides[$i]['mcontainer_right'] = strip_tags( get_post_custom_values("mcontainer_right")[0] );

		$slides[$i]['image'] = get_the_post_thumbnail_url();
		$i++; 
		endwhile;
	endif;

?>

<script>var slides = <?= json_encode($slides); ?>;</script>

<div class="pcontainer">
    <div class="ucontainer"></div>
    <div class="threeword">
        <div class="mcontainer left"></div>
        <div class="mcontainer middle"></div>
        <div class="mcontainer right"></div>
    </div>
	<div class="slideButton previous"></div>
	<div class="slideButton next"></div>
    <div class="dcontainer"></div>
</div>
<div class="parallaxImageBG"></div>
<div class="parallaxImageBGtransition"></div>

<div class="contentBackground">
	<div id="content">
		
		<div class="row">
			
			<?php 
				$nocteamposts = array();
				
				$nocPosts = new WP_Query(
					array
					(
						"cat" => get_category_by_slug('noc')->cat_ID
					)
				);
			?>
			
			<?php while ($nocPosts->have_posts()) : $nocPosts->the_post() ?>
		
			<div class="column column-4">
				<div class="title"> 
					<h4><?= get_the_title(); ?></h4> 
				</div>
				
				<div class="body">
					<?= get_the_content('',FALSE,'')  ?>
				</div>
				
				<div class="cardButton link">
					<a href="<?= get_permalink() ?>">More</a>	
				</div>		
			</div>
			
			<?php endwhile; ?>
	
		</div>
	
		
	</div>
</div>

<?php get_sidebar(); ?>

<?php get_footer(); ?>