$(document).ready(
    
    widthThreshold = "928", 
    isOnMobile = window.outerWidth > widthThreshold,
    hoveredItem = null,
    toggledDropdowns = [],
    tLogoPressStart = 0,
    tLogoPressLong = "2", //seconds after logo is pressed
    tLogoPressLast = 0, //mouseleave - mousedown 
    browserWidth = 0,
    navbarHoverTimeout = null,
    scrolledPx = $(window).scrollTop(),
    
    $("#main_navbar .sandwich").on( 
        "click",
        function() {
            $("#main_navbar .hmenu").slideToggle();
        }
    ),
    
    $(window).ready(
        function()
        {
            browserWidth = $(window).outerWidth;
            
            multiDepthMenu = $("#main_navbar .header-menu.main li");

            multiDepthMenu.children("ul.sub-menu").parent().children("a").css(
                {
                    width: "initial", 
                    height: "70px"
                }
            );
            
        }    
    ),
    
    $(window).resize(
        function() 
        {
            if (window.outerWidth != browserWidth)
            {
                isOnMobile = (window.outerWidth > widthThreshold);
                
                if (isOnMobile)
                {
                    $("#main_navbar .hmenu").show();
                    $.each(
                        toggledDropdowns, 
                        function( index, value ) 
                        {
                            value.hide();
                        }
                    );
                    toggledDropdowns.splice(0, toggledDropdowns.length);
                }
                else
                {
                    $("#main_navbar .hmenu").hide();
                    $("#main_navbar .header-menu.main li:hover .sub-menu").hide();
                }
                browserWidth = window.outerWidth;
            }
            
        }
    ),
    
    $("#main_navbar .header-menu.main li").on(
        "click",
        function()
        {
            if (!isOnMobile)
            {
                toggledDropdowns.push( $("#main_navbar .header-menu.main li:hover .sub-menu") );
                $.uniqueSort( toggledDropdowns );
                
                $("#main_navbar .header-menu.main li:hover .sub-menu").slideToggle()
            }
        }
    ),
    
    $("#main_navbar .header-menu.main li").on(
        {
            mouseenter: function () 
            {
                if (isOnMobile)
                {
                    if (hoveredItem != null) 
                    {
                        hoveredItem.hide();
                    }
                    clearTimeout(navbarHoverTimeout);
                    hoveredItem = $("#main_navbar .header-menu.main li:hover .sub-menu");
                    hoveredItem.show();
                }
            },
            mouseleave: function () 
            {
                if (isOnMobile)
                {
                    if (hoveredItem != null)
                    {
                        if (hoveredItem.length != 0)
                        {
                            navbarHoverTimeout = setTimeout(
                                function()
                                {
                                    hoveredItem.hide();
                                    hoveredItem = null;
                                },
                                200
                            );
                        }
                    }
                }
            }
        }
    ),
    
    $("#main_navbar .logo").on(
        "mousedown touchstart",
        function()
        {
            tLogoPressStart = Date.now();
            $("#main_navbar .logo .mask").addClass("active");
            $("#main_navbar .logo .refresh_anim").addClass("active");
        }
    ),
    
    $("#main_navbar .logo").on(
        "mouseup touchend",
        function()
        {
            //TODO: Debug this because it doesn't seem to work
            tDelta = Date.now() - (tLogoPressStart + tLogoPressLast);
            if (tDelta <= tLogoPressLong*1000)
            {
                window.location.href = window.location.pathname.substr(0);
            }
            else
            {
                window.location.reload();
            }
            $("#main_navbar .logo .mask").removeClass("active");
            $("#main_navbar .logo .refresh_anim").removeClass("active");
        }
    ),
    
    $("#main_navbar .logo").on(
        "mouseleave",
        function()
        {
            $("#main_navbar .logo .mask").removeClass("active");
            $("#main_navbar .logo .refresh_anim").removeClass("active");
            tLogoPressLast = Date.now() - tLogoPressStart;
        }
    ),
    
    $(".backToTopButton").on(
        "click",
        function()
        {
            $('html,body').animate({ scrollTop: 0 }, 'slow');
        }
    ),
    
    $(window).on(
        "scroll",
        function()
        {
            scrolledPx = $(window).scrollTop();
            
            if (scrolledPx > 10)
            {
                $(".backToTopButton").fadeIn();
            }
            else
            {
                $(".backToTopButton").fadeOut();
            }
        }
    )
);
