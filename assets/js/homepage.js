var homepageSlider = {
    
    sliderInterval: 10000,
    animationsTime: 2500,
    currentSlide: 0,
    sliderTimer: null,

    switchBackground: function(newImage)
    {
        var imgURLstring = "url(" + newImage + ")";
        
        $(".parallaxImageBGtransition").css("background-image", imgURLstring);
        
        $(".parallaxImageBGtransition").stop().fadeIn(
            this.animationsTime, 
            function()
            {
                $(".parallaxImageBG").css("background-image", imgURLstring)
                $(".parallaxImageBGtransition").hide();
            }
        );
    },
    
    nextSlide: function()
    {
        if (this.currentSlide < slides.length -1)
        {
            this.currentSlide++;
        }
        else
        {
            this.currentSlide = 0;
        }
        
        this.updateSlide();
    },
    
    previousSlide: function()
    {
        if (this.currentSlide > 0)
        {
            this.currentSlide--;
        }
        else
        {
            this.currentSlide = slides.length - 1;
        }
        
        this.updateSlide();
    },
    
    updateSlide: function()
    {
        $(".pcontainer > .ucontainer").text( slides[this.currentSlide].title );
        
        $(".pcontainer > .threeword > .mcontainer.left").text( slides[this.currentSlide].mcontainer_left );
        $(".pcontainer > .threeword > .mcontainer.middle").text( slides[this.currentSlide].mcontainer_center );
        $(".pcontainer > .threeword > .mcontainer.right").text( slides[this.currentSlide].mcontainer_right );
        
        $(".pcontainer > .dcontainer").text( slides[this.currentSlide].content );
        
        this.switchBackground( slides[this.currentSlide].image )
    },
    
    resetSliderTimer: function()
    {
        clearInterval(this.sliderTimer);
        
        this.sliderTimer = setInterval(
            function()
            {
                homepageSlider.nextSlide();
            }, this.sliderInterval
        );
    }

};

$( document ).ready(

    $(".pcontainer > .slideButton.next").on(
        "click",
        function(e)
        {
            homepageSlider.nextSlide();
            homepageSlider.resetSliderTimer();
        }
    ),
    
    $(".pcontainer > .slideButton.previous").on(
        "click",
        function(e)
        {
            homepageSlider.previousSlide();
            homepageSlider.resetSliderTimer();
        }
    ), 
    
    $( window ).ready(
        function()
        {
            slides = $.makeArray(slides);
            homepageSlider.updateSlide();
            
            homepageSlider.resetSliderTimer();
        }
    )
)