function MFrame() 
{
    //The state of the frame
    this.isShown = false;
    
    //Method that closes the frame
    this.closeFrame = function closeFrame() 
    {
        if (this.isShown)
        {
            $(".materialOverlay").fadeOut();
            this.isShown = false;
        }
    }
    
    //Method that shows the frame
    this.openFrame = function openFrame() 
    {
        if (!this.isShown)
        {
            $(".materialOverlay").fadeIn();
            this.isShown = true;
        }
    }
    
    //Input: STRING, HTML. Takes both arguements and attempts to display them to the frame.
    this.showMessage = function showMessage(title, message)
    {
        //Clean up all the useless spaces
        title = title.replace(/ +(?= )/g,'');
        
        //Check if title is over the character limit
        if (title.length > 20)
        {
            //Substring the title
            title = title.substr(0,23);
            
            //Trim again the string to remove cutted-in-half words and add 3 dots in the end
            title = title.substr(0, Math.min(title.length, title.lastIndexOf(" "))) + "...";
        }
        
        //Set the string title
        $(".materialOverlay .materialFrame .head .title").text(title);
        
        //Set the html body
        $(".materialOverlay .materialFrame .body").html(message);
        
        //Show the frame in case its closed
        this.openFrame();
    }
}


////////// EVENT REGISTRATIONS //////////

//TODO: Find a cleaner and nicer name here. This is the main frame but i am pretty sure you don't want to call it the mainframe. Right?
var myMFrame1 = new MFrame();

//On user clicking the infamous X button.. 
$(".materialOverlay .materialFrame .head .closeBtn").on(
	"click",
	function(e)
	{
        //Hide the frame
		myMFrame1.closeFrame();
	}	
)