$(document).ready(
  
    sidebarShown = false,
    sidebarTimeout = null,
    
    initiateHideSidebarButtonSequence = function()
    {
        clearTimeout(sidebarTimeout);
        if (window.outerWidth <= "1455")
        {
            $("#sidebar .activityHubCircularButton").fadeIn();
        }
        
        if (sidebarShown == false)
        {
            sidebarTimeout = setTimeout(
                function()
                {
                    $("#sidebar .activityHubCircularButton").fadeOut();
                }, 5000
            );
        }
    },

    $(window).on(
        "scroll",
        function()
        {
            this.initiateHideSidebarButtonSequence();
        }
    ),

    $("#sidebar .activityHubCircularButton").on(
        {
            mouseenter: function () 
            {
                clearTimeout(sidebarTimeout);
            },
            mouseleave: function () 
            {
                initiateHideSidebarButtonSequence();
            }
        }
    ),

    $("#sidebar .activityHubCircularButton").on(
        "click",
        function(e)
        {
            initiateHideSidebarButtonSequence();
            if (sidebarShown != null)
            {
                sidebar = $("#sidebar");
                sidebarShown = sidebar.hasClass("active");
                
                if (sidebarShown)
                {
                    sidebar.removeClass("active");
                }
                else
                {
                    sidebar.addClass("active");
                }

                sidebarShown = !sidebarShown;
            }
        }
    ),
    
    $(window).ready(
        this.initiateHideSidebarButtonSequence()
    )
);