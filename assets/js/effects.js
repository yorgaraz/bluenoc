var rElement, ink, d, x, y;

function doRipple(rElement, e)
{
	
	//If ink doesn't exist, appent to the element as a child
	if(rElement.find(".ink").length == 0)
		rElement.append("<span class='ink'></span>");
	
	//find the ink
	ink = rElement.find(".ink");
	
	//remove it's animate class
	ink.removeClass("animate");
	
	//Initialize the correct width and height attributes of the ink
	if(!ink.height() && !ink.width())
	{
		d = Math.max(rElement.outerWidth(), rElement.outerHeight());
		ink.css({height: d, width: d});
	}
	
	//Fetch interaction(mouse/touch) position relative to the client's window minus the radius of the ink size
	x = e.clientX - ink.outerWidth()/2;
	y = e.clientY - ink.outerHeight()/2;
	
	/*
	* We check which side of the element (we want to apply the ink rotation) is larger (height or width) and we store the size of it 
	* The size will later determine the radius of the ink
	*/
	elementSize = ( rElement.width() > rElement.height() ) ? rElement.width() : rElement.height();
	
	//Create the ink with the position user clicked and the size determined by the elementSize variable
	ink.css(
		{
			top: y + 'px', 
			left: x + 'px', 
			width: elementSize + "px", 
			height: elementSize + "px"
		}
	).addClass("animate");
	
}

////////// EVENT REGISTRATIONS //////////

$("#sidebar .activityHubCircularButton").on(
	"click",
	function(e)
	{
		doRipple($(this), e);
	}
)

$(".backToTopButton").on(
	"click",
	function(e)
	{
		doRipple($(this), e);
	}
)

$("#main_navbar .sandwich").on(
	"click",
	function(e)
	{
		doRipple($(this), e);
	}
)

$(".pcontainer .slideButton").on(
	"click",
	function(e)
	{
		doRipple($(this), e);
	}
)