//Variable to store the timeout sequence for auto rotation
var threejs_networkmap_autorotateTimeout = null;

//Fetch the width and height of the card that the renderer will be placed at
var viewport_width = $('.webglviewport').outerWidth();
var viewport_height = $('.webglviewport').css("height").replace(/[^0-9]/g, '');

//Get a renderer with transparent abilities
var renderer = new THREE.WebGLRenderer({ alpha: true });

//Get a scene
var scene = new THREE.Scene();

//Get a camera with FOV 70, Aspect ratio of the viewport, Near cut-off 0.1, far cut-off 1000000 (aka draw distance)
var camera = new THREE.PerspectiveCamera( 70, viewport_width / viewport_height, 0.1, 1000000 );

//Setup the light for our scene
var dl1 = new THREE.DirectionalLight( 0xffffff, 0.8 );

//Set camera controls
var controls = new THREE.OrbitControls( camera, renderer.domElement );

//Event listener for window resizing
window.addEventListener('resize', function(event){
    //get width and height of the current card
    viewport_width = $('.webglviewport').outerWidth();
    viewport_height = $('.webglviewport').css("height").replace(/[^0-9]/g, '');
    
    //reset the size of the renderer and the aspect ratio of the camera
    renderer.setSize( viewport_width, viewport_height );
    camera.aspect = viewport_width / viewport_height;
});

function initialize()
{
    //set the initial renderer size
    renderer.setSize( viewport_width, viewport_height );
    
    //enable use of soft shadows by the renderer
    renderer.shadowMap.type = THREE.PCFSoftShadowMap;

    //Append the renderer to the card
    $('.webglviewport').append( renderer.domElement );
    
    //Set camera position and rotation
    camera.rotation.x = -44.8;
    camera.position.y = 200;
    camera.position.z = 300;
    
    //Configure controls
    controls.target.set(0, 0, 0)
    controls.minDistance = 200;
    controls.maxDistance = 400;
    controls.autoRotate = true;

    //Load the schooltest1 file
    function loadSchoolScene()
    {
        //Get a new scene loader
        var loader = new THREE.SceneLoader();

        //Access load and get the schooltest1 to loadschoolcallback
        loader.load
        (
            "wp-content/themes/bluenoc/assets/other/schooltest1.js",
            loadSchoolCallback
        );

        //load the school scene
        function loadSchoolCallback(result) 
        {     
            //Replace the existing scene with the scene loaded  
            scene = result.scene;
            
            //More settings for our light + add it to the scene
            dl1.position.set( 0, 1, 0 );
            dl1.castShadow = true;
            dl1.shadow.camera = true;
            dl1.shadow.camera.right     =  5;
            dl1.shadow.camera.left    = -5;
            dl1.shadow.camera.top      =  5;
            dl1.shadow.camera.bottom   = -5;
            scene.add(dl1);
            
            //Search through the scene
            scene.traverse( 
                function( node ) 
                {
                    //for nodes of type THREE.Mesh type
                    if ( node instanceof THREE.Mesh ) 
                    {
                        //Enable shadows + transparency
                        node.material.transparent = true;
                        node.material.opacity = 0.75;
                        node.castShadow = true;
                        node.receiveShadow = false;
                    }
                } 
            );
        }
    }
    
    //Execute loadSchoolScene()
    loadSchoolScene();
}

function update()
{
    //Preinitialize the tPrevious
    var tPrevious = Date.now();

    //Makes the necessary updates for each new frame
    function render() 
    {
        //Get the time delta between the frames
        var tDelta = Date.now() - tPrevious;
        
        //update the controls
        controls.update();
        
        //update the directional light position to follow camera position
        dl1.rotation.x = camera.rotation.x;
        dl1.rotation.y = camera.rotation.y;
        dl1.rotation.z = camera.rotation.z;
        
        //Request next animation
        requestAnimationFrame( render );
        
        //Render the scene with the camera
        renderer.render( scene, camera );
        
        //Update tPrevious ( of tDelta )
        tPrevious = Date.now();
    }
    
    //Is called for each frame
    render();
}

initialize();
update();

////////// LISTENERS //////////

// On controls interaction, stop autorotate
controls.addEventListener('start', function(){
  clearTimeout(threejs_networkmap_autorotateTimeout);
  controls.autoRotate = false;
});

// after 2 seconds, resume autorotate
this.controls.addEventListener('end', function(){
  threejs_networkmap_autorotateTimeout = setTimeout(function(){
    controls.autoRotate = true;
  }, 2000);
});