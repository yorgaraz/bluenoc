//Collapse - Expand mechanism
$(".column .item .itemTitle .collapseButton").on(
    "click",
    function(e)
    {
        $(this).parent().siblings(".itemBody").slideToggle();
        $(this).text( ( ($(this).text() == "-") ? "+" : "-") ); 
    }
)

//Frame mechanism
$(".column .item .itemBody .guide .button").on(
    "click",
    function(e)
    {
        //Selects the title of the item. .get(2) is fetching the ".body" without ".collapseBtn".
        var itemTitleSelector = $(this).parent().parent().siblings(".itemTitle").contents().get(2);
        var itemTitle = $( itemTitleSelector ).text();
        
        var itemBody = $(this).siblings(".body").html();
        myMFrame1.showMessage(itemTitle, itemBody);
    }
)