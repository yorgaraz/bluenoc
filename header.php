<?php

/**
 * The template for displaying the header
 *
 * Displays all of the head element and everything up until the "site-content" div.
 *
 * @package WordPress
 * @subpackage bluenoc
*/
?>

<?php 

////////// NAVBAR MAIN HEADER CONFIGURATION //////////

$header_configuration = array(
	'theme_location'  => 'header-menu',
	'items_wrap'      => '<ul class="header-menu main" lang="el">%3$s</ul>',
	'depth'           => 0,
	'container_class' => 'menu-header-container',
	'container'       => 'div',
);	
	
?>

<!DOCTYPE html>

<html <?php language_attributes(); ?>>

<head>
	<meta charset="<?php bloginfo('charset'); ?>" name="viewport" content="width=device-width, initial-scale=1" />
	<meta name="theme-color" content="#0277BD" />

	<title><?php wp_title(); ?></title>
	<?php wp_head(); ?>
</head>

<body>

<div class="materialOverlay">
	<div class="materialFrame" id="0">
		<div class="head">
			<div class="title"></div>
			<div class="closeBtn">X</div>
		</div>
		<div class="body">
			
		</div>
	</div>
</div>

<div id="main_navbar">
	<div class="logo">
		<div class="mask"></div>
		<div class="refresh_anim"></div>
	</div>
    <a href="#" class="sandwich">≡</a>
    <div class="hmenu">
	<?php wp_nav_menu( $header_configuration ); ?>
    </div>
</div>

<div class="backToTopButton"></div>