<?php

    $footer_configuration = array(
        'theme_location'  => 'footer-menu',
        'depth'           => 1,
        'container_class' => 'menu-footer-container',
    );	
    
?>

<?php $pFooter = get_page_by_title( 'Footer' ); ?>
<?php $qData = new WP_Query("page_id=" . $pFooter->ID); ?>
<?php while ($qData->have_posts()) : $qData->the_post(); ?>
<footer id="footer">
    <div class="container">
        <?php $logoURL = get_template_directory_uri() . "/" . get_post_meta(get_the_ID(), 'logoImageURL', true); ?>
        <?php if ( empty($logoURL) ): ?>
            <div class="blockleft" style="background: url( <?= $logoURL ?> ); background-size: cover;"></div>
        <?php else: ?>
            <div class="blockleft"></div>
        <?php endif; ?>
        <div class="blockright">
            <?php the_content(); ?>
            <br>
            <?php                 
                wp_nav_menu( $footer_configuration );
            ?>
        </div>
    </div>
</footer>
<?php endwhile; ?>
<?php wp_footer(); ?>
</body>
</html>