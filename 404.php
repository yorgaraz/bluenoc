<?php get_header(); ?>

<div class="parallaxImageBG"></div>

<div class="contentBackground">
	<div id="content">
		
		<div class="row">			
		
			<div class="column column-12">
				<div class="title"> 
					<h4>404</h4> 
				</div>
				
				<div class="body">
					<h1>Whoops! It looks like we were not able to find this page<h1>
					<div class="sadface">:(</div>
				</div>			
			</div>
	
		</div>
	
		
	</div>
</div>

<?php get_sidebar(); ?>

<?php get_footer(); ?>