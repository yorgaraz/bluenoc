<?php get_header(); ?>

<div id="content">
	
	<div class="row">
		<div class="column column-12">
			
			<?php while (have_posts()) : the_post(); ?>
			
			<div class="title"> 
				<h4><?php the_title(); ?></h4> 
			</div>
			
			<div class="body">
				<?php the_content(); ?>
			</div>
			
			<?php endwhile;?>
			
		</div>
	</div>
	
</div>




<?php get_sidebar(); ?>

<?php get_footer(); ?>