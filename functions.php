<?php

////////// CUSTOM QUERY FUNCTIONS /////////

/*
*   Builds a custom query based on GET requests and the current category
*   Search and pagination are included 
*/
if ( !function_exists("bluenoc_generateCategoryItemList") )
{
    function bluenoc_generateCategoryItemList()
    {
        //This is the array in which the information from the query will be stored
        $out = Array();
        
        //Stores the current category id
        $out['currentCategoryID'] = get_query_var('cat');
        
        //Stores the current page (for pagination) from GET
        $out['currentPage'] = $_GET['pg'];
        
        //If current page GET variable wasn't available, set current page to 1
        if ( empty($out['currentPage']) )
        {
            $out['currentPage'] = 1;
        }
        
        /*
        *   BEGIN OF HIDDEN INPUT FORMS
        *
        *   If the cat GET variable was already specified in the current page instance 
        *   Then hiddenInputStr will be filled with a hidden input form with the current cat value
        *   This happens so that no current _GET variable is lost to any hyperlinks  
        *   that are generated from this action. 
        *   
        *   TODO: Recode this part of the code so that all potential _GET variables are taken into account
        */
        
        $out['hiddenInputStr'] = "";
        
        if (!empty($_GET['cat']))
        {
            $out['hiddenInputStr'] = '<input type="hidden" name="cat" value="' . $_GET['cat'] . '" />';
        }
        
        //  END OF HIDDEN INPUT FORMS
        
        //Build up the search string
        $out['searchStr'] = "";
        if (!empty($_GET['s']))
        {
            $out['searchStr'] = $_GET['s'];
        }
        
        /*
        *   BEGIN OF QUERY CREATION
        *
        *   TODO: Make posts_per_page accessible to configuration
        */
        $out['theSearch'] = new WP_Query( 
            array
            ( 
                's' => $out['searchStr'],
                'post_type' => 'post',
                'cat' => $out['currentCategoryID'],
                'posts_per_page' => 10,
                'paged' => $out['currentPage']
            ) 
        );  
        /*
        *   END OF QUERY CREATION
        */ 

        //Counts all the posts of the given search string. Pagination excluded from the equation.
        $out['theSearch_postCount'] = count( get_posts('post_type=post&category=' . $out['currentCategoryID'] . '&s=' . $out['searchStr'] . '&posts_per_page=-1' ));
        //Calculates the maximum number of pages for the given search string.
        $out['theSearch_page_num_max'] = (int)ceil($out['theSearch_postCount'] / $out['theSearch']->query['posts_per_page']);
        
        //Calculates the next page from the current page
        $out['next_page_num'] = ( $out['currentPage'] + 1 > $out['theSearch_page_num_max'] ) ? 1 : $out['currentPage'] + 1 ;
        //Calculates the previous page from the current page
        $out['prev_page_num']  = ( $out['currentPage'] - 1 < 1 ) ? $out['theSearch_page_num_max'] : $out['currentPage'] - 1 ;
        
        /*
        *   BEGIN OF pagination links building
        */
        $out['next_page_URL'] = "";
        $out['prev_page_URL'] = "";
        
        if ( empty($_GET['pg']) )
        {
            $out['next_page_URL'] .= $_SERVER['REQUEST_URI'] . "&pg=" . $out['next_page_num'];
            $out['prev_page_URL'] .= $_SERVER['REQUEST_URI'] . "&pg=" . $out['prev_page_num'];
        }
        else 
        {
            $out['next_page_URL'] .= preg_replace("/&pg=[0-9]{0,}/", '&pg=' . $out['next_page_num'], $_SERVER['REQUEST_URI']);
            $out['prev_page_URL'] .= preg_replace("/&pg=[0-9]{0,}/", '&pg=' . $out['prev_page_num'], $_SERVER['REQUEST_URI']);        
        }
        /*
        *    END OF pagination links building
        */
        
        //If any errors occur append them to this string.
        $out['errorMsg'] = "";

        /*
        *   Check if posts exist with given criterea
        *
        *   TODO: Remove hardcoding-
        */
        if ( count($out['theSearch']->posts) == 0 )
        {
            if (!empty( $out['searchStr'] ))
            {
                $out['errorMsg'] .= '<p>There are no posts matching "' . $out['searchStr'] . '"</p>';
            }
            else 
            {
                $out['errorMsg'] .= '<p>This category has no entries.</p>';
            }
        }
        
        //Return results
        return $out;
    }
    
}

if ( !function_exists("bluenoc_initiateQuerySetup") )
{
    function bluenoc_initiateQuerySetup()
    {
        return bluenoc_generateCategoryItemList();
    }
    
    add_filter( 'bluenoc_initiateQuerySetupFilter', 'bluenoc_initiateQuerySetup', 10, 0 );
}

////////// THEME SUPPORT //////////

function additional_support_features() {
    add_theme_support( 'post-thumbnails' ); 
    set_post_thumbnail_size( 1440, 600, false );
    add_image_size( 'header-bg', 4096, 600 );
}
add_action( 'after_setup_theme', 'additional_support_features' );

////////// ADD STYLES //////////

function bluenoc_enqueue_styles()
{
    wp_enqueue_style('style', get_stylesheet_uri());
    wp_enqueue_style('gridsystem', get_template_directory_uri() . '/assets/css/gridsystem.css');
    wp_enqueue_style('navbars', get_template_directory_uri() . '/assets/css/navbars.css');
    wp_enqueue_style('effects_style', get_template_directory_uri() . '/assets/css/effects.css');
    wp_enqueue_style('content', get_template_directory_uri() . '/assets/css/content.css');
    wp_enqueue_style('sidebar', get_template_directory_uri() . '/assets/css/sidebar.css');
    wp_enqueue_style('footer', get_template_directory_uri() . '/assets/css/footer.css');
    wp_enqueue_style('hacks', get_template_directory_uri() . '/assets/css/hacks.css');
    wp_register_style('categoryDefault', get_template_directory_uri() . '/assets/css/category_default.css');
    wp_register_style('categoryContact', get_template_directory_uri() . '/assets/css/category_contact.css');
    wp_register_style('404', get_template_directory_uri() . '/assets/css/404.css');
    wp_register_style('staticHomepage', get_template_directory_uri() . '/assets/css/static_homepage.css');
    wp_register_style('networkmapViewport', get_template_directory_uri() . '/assets/css/networkmap_viewport.css');
    
    //if there is any category and the page isn't home append categoryDefault
    if( has_category() && !is_home() )
    {
        wp_enqueue_style('categoryDefault');
    }
    
    // If the category is contact apply category-contact specific styles
    if ( is_category( 'contact' ) )
    {
        wp_enqueue_style('categoryContact');
    }
    
    // If its home contact apply homepage specific styles
    if ( is_home() )
    {
        wp_enqueue_style('staticHomepage');
    }
    
    // If the category is infrastructure add networkmap_viewport.css
    if ( is_category( 'infrastructure' ) )
    {
        wp_enqueue_style('networkmapViewport');
    }
    
    // If it is 404
    if ( is_404() )
    {
        wp_enqueue_style('404');
    }
}

add_action('wp_enqueue_scripts', 'bluenoc_enqueue_styles');

////////// ADD SCRIPTS //////////

function bluenoc_threejs_scripts()
{
    wp_register_script('threejs', get_template_directory_uri() . '/assets/js/three.js', array(), '0.7.5', true );
    wp_register_script('threejs_pointmaterial', get_template_directory_uri() . '/assets/js/PointsMaterial.js', array(), '0.7.0', true );
    wp_register_script('threejs_sceneLoader', get_template_directory_uri() . '/assets/js/sceneLoader.js', array(), '0.7.0', true );
    wp_register_script('threejs_orbitControls', get_template_directory_uri() . '/assets/js/OrbitControls.js', array(), '0.7.0', true );
    
    // If the category is infrastructure or home add three.js
    if ( is_category( 'infrastructure' ) )
    {
        wp_enqueue_script('threejs');
        wp_enqueue_script('threejs_pointmaterial');
        wp_enqueue_script('threejs_sceneLoader');
        wp_enqueue_script('threejs_orbitControls');
    }
}

function bluenoc_enqueue_scripts()
{
    wp_enqueue_script('jquerycustom', get_template_directory_uri() . '/assets/js/jquery.js', array(), '2.2.3', true );
    wp_enqueue_script('navbar', get_template_directory_uri() . '/assets/js/navbar.js', array(), '0.4.2', true );
    wp_enqueue_script('effects_script', get_template_directory_uri() . '/assets/js/effects.js', array(), '0.1.0', true );
    wp_enqueue_script('sidebar_script', get_template_directory_uri() . '/assets/js/sidebar.js', array(), '0.1.0', true );
    wp_enqueue_script('category_faq', get_template_directory_uri() . '/assets/js/category_default.js', array(), '0.1.0', true );
    wp_enqueue_script('materialFrameManager', get_template_directory_uri() . '/assets/js/materialFrameManager.js', array(), '0.1.0', true );
    wp_register_script('homepage', get_template_directory_uri() . '/assets/js/homepage.js', array(), '0.1.0', true );
    
    bluenoc_threejs_scripts();
    
    // If the category is infrastructure or home add three.js
    if ( is_home() )
    {
        wp_enqueue_script('homepage');
    }
}

add_action('wp_enqueue_scripts', 'bluenoc_enqueue_scripts');

//////////  REGISTER MENUS //////////

function bluenoc_menu_registration()
{
    register_nav_menus(
        array(
            'header-menu' => __( 'Header Menu' ),
            'header-submenu' => __( 'Header Submenu' ),
            'footer-menu' => __( 'Footer Menu' )
        )
    );
}
add_action( 'init', 'bluenoc_menu_registration' );