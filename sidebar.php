<?php 

function excerpt_text($text, $characters=75)
{
    $excerpt = preg_replace(" (\[.*?\])", '', $text);
    $excerpt = strip_shortcodes( $excerpt );
    $excerpt = strip_tags( $excerpt );
    $excerpt = substr( $excerpt, 0, $characters );
    $excerpt = substr( $excerpt, 0, strripos( $excerpt, " " ) );
    $excerpt = trim( preg_replace( '/\s+/', ' ', $excerpt ) );
    $excerpt .= "...";
    
    return $excerpt;
}

?>

<div id="sidebar">
    <div class="activityHubCircularButton"></div>
	<div class="title">
		Activity Hub
	</div>
	<div class="announcements">
		
		<h4>Ανακοινώσεις</h4>
		
		<?php query_posts('category_name=announcements'); ?>
		<?php while (have_posts()) : the_post(); ?>
		
        <a href="<?= get_permalink() ?>">
		<div class="announcement">
            <div class="body">
			    <?= excerpt_text(get_the_content()); ?>
            </div>
            <div class="sourceicon">
                <img class="icon" src="wp-content/themes/bluenoc/assets/images/icons/ic_language_white_48dp_2x.png"></img>
            </div>
			<div class="timePosted"><?= human_time_diff( get_post_time(), current_time('timestamp')  ); ?> ago</div>
		</div>
        </a>
		
		<?php endwhile; ?>
		
	</div>
	<div class="social">
		<div class="facebook"></div>
        <div class="twitter"></div>
        <div class="google"></div>
	</div>
</div>